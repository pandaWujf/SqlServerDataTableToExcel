﻿using System.Collections.Generic;

namespace DataTableToExcel.Application.Shared.SqlAdo.SqlSourceDtos
{
    public class DataTableDto
    {
        public string TableName { get; set; }

        public string TableType { get; set; }
        public string TableID { get; set; }

        /// <summary>
        /// 表的主键字段
        /// </summary>
        public string KeyName { get; set; }


        public string TableExplain { get; set; }

        public int ExcelIndex { get; set; }

        public List<TableColDto> TableCols { get; set; }
    }
}
