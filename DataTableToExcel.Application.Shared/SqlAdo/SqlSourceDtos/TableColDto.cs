﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTableToExcel.Application.Shared.SqlAdo.SqlSourceDtos
{
    public class TableColDto
    {
        public string ColName { get; set; }

        public string ColType { get; set; }

        public string ColLength { get; set; }

        public string DecimalLength { get; set; }

        public bool ColNull { get; set; }

        public string ColDefault { get; set; }

        public bool ISKey { get; set; }

        public string ColExp { get; set; }
    }
}
