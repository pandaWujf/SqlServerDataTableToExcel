﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTableToExcel.Application.Shared.Admin.DataListDtos
{
    public class CreateOrEditDataListDto
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid? Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DataSource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Ds_Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Ds_Pwd { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FileName { get; set; }
    }
}
