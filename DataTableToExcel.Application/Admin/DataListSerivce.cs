﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTableToExcel.Application.Shared.Admin.DataListDtos;
using DataTableToExcel.Entity;
using DataTableToExcel.Entity.Admin;

namespace DataTableToExcel.Application.Admin
{
    public class DataListSerivce
    {

        private readonly DteDbContext _dteDbContext;

        public DataListSerivce()
        {
            _dteDbContext = new DteDbContext();

            _dteDbContext.Database.EnsureCreated();

        }

        public List<DataList> GetAll()
        {
            var dataLists = _dteDbContext.DataLists.ToList();

            return dataLists;
        }

        public DataList CreateOrUpdate(CreateOrEditDataListDto input)
        {
            DataList dataList;
            if (input.Id == null)
            {
                var existDataList = _dteDbContext.DataLists.Where(a => a.Name == input.Name).FirstOrDefault();

                if (existDataList != null)
                {
                    throw new Exception("名称已经存在");
                }

                dataList = new DataList();
                dataList.Id = Guid.NewGuid();
                dataList.CreateDate = DateTime.Now;
                dataList.Name = input.Name;
                dataList.Type = input.Type;
                dataList.IpAddress = input.IpAddress;
                dataList.DataSource = input.DataSource;
                dataList.Ds_Name = input.Ds_Name;
                dataList.Ds_Pwd = input.Ds_Pwd;
                dataList.FileName = input.FileName;

                _dteDbContext.Add(dataList);
            }
            else
            {
                var existDataList = _dteDbContext.DataLists.Where(a => a.Name == input.Name && a.Id != input.Id).FirstOrDefault();
                if (existDataList != null)
                {
                    throw new Exception("名称已经存在");
                }

                dataList = _dteDbContext.DataLists.Where(a => a.Id == input.Id).FirstOrDefault();
                dataList.CreateDate = DateTime.Now;
                dataList.Name = input.Name;
                dataList.Type = input.Type;
                dataList.IpAddress = input.IpAddress;
                dataList.DataSource = input.DataSource;
                dataList.Ds_Name = input.Ds_Name;
                dataList.Ds_Pwd = input.Ds_Pwd;
                dataList.FileName = input.FileName;

                _dteDbContext.Update(dataList);
            }

            _dteDbContext.SaveChanges();

            return dataList;

        }

        public void UpdateFileName(Guid id, string fileName)
        {
            var dataList = _dteDbContext.DataLists.Where(a => a.Id == id).FirstOrDefault();
            dataList.FileName = fileName;
        }


        public void Delete(Guid id)
        {
            var entity = _dteDbContext.DataLists.Where(a => a.Id == id).FirstOrDefault();
            if (entity != null)
            {
                _dteDbContext.DataLists.Remove(entity);
            }

            _dteDbContext.SaveChanges();
        }
    }
}
