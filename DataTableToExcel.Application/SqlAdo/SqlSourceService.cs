﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTableToExcel.Application.Shared.Common;
using DataTableToExcel.Application.Shared.SqlAdo.SqlSourceDtos;
using DataTableToExcel.Application.SqlAdo.Domain;

namespace DataTableToExcel.Application.SqlAdo
{
    public class SqlSourceService
    {
        public ISqlSourceManager _sqlSourceManager;

        public SqlSourceService(string type, string ipAddress, string userName, string userPwd)
        {
            if (type == TypeConsts.SqlServer)
            {
                _sqlSourceManager = new SqlServerSourceManager(ipAddress, userName, userPwd);
            }

            if (type == TypeConsts.MySql)
            {
                _sqlSourceManager = new MySqlSourceManager(ipAddress, userName, userPwd);
            }
        }


        public List<string> GetDataSourceList()
        {
            return _sqlSourceManager.GetDataSourceList();
        }

        public List<DataTableDto> GetTableMsg(string DataSource)
        {
            return _sqlSourceManager.GetTableMsg(DataSource);
        }

    }
}
