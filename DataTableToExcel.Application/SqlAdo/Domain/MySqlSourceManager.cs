﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTableToExcel.Application.Shared.SqlAdo.SqlSourceDtos;
using SqlSugar;

namespace DataTableToExcel.Application.SqlAdo.Domain
{
    public class MySqlSourceManager : ISqlSourceManager
    {

        private readonly SqlSugarClient _sqlSugarClient;

        public MySqlSourceManager(string ipAddress, string userName, string userPwd)
        {
            string dataSource = "information_schema";
            ConnectionConfig connectionConfig = new ConnectionConfig();
            connectionConfig.DbType = SqlSugar.DbType.MySql;
            connectionConfig.ConnectionString = $"server={ipAddress};Database={dataSource};User Id={userName};password={userPwd};Charset=utf8";

            _sqlSugarClient = new SqlSugarClient(connectionConfig);

        }
        public List<string> GetDataSourceList()
        {
            List<string> rtn = new List<string>();

            string sql = "show databases";
            DataTable dt = _sqlSugarClient.Ado.GetDataTable(sql);
            foreach (DataRow dr in dt.Rows)
            {
                rtn.Add(dr["Database"].ToString());
            }

            return rtn;
        }

        public List<DataTableDto> GetTableMsg(string dataSource)
        {

            List<DataTableDto> rtn = new List<DataTableDto>();
            string sql = $@"SELECT  TABLE_NAME ,TYPE,TABLE_COMMENT FROM (
SELECT  TABLE_NAME,'表' as TYPE,TABLE_COMMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '{dataSource}' and TABLE_TYPE='BASE TABLE'
union all
SELECT TABLE_NAME,'视图' as TYPE,'' AS TABLE_COMMENT FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_SCHEMA = '{dataSource}' ) A 
order by TYPE,TABLE_NAME";

            DataTable tbNames = _sqlSugarClient.Ado.GetDataTable(sql);

            sql = $@"select  TABLE_NAME,COLUMN_NAME,ORDINAL_POSITION,COLUMN_COMMENT,IS_NULLABLE,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,COLUMN_DEFAULT,COLUMN_KEY from information_schema.columns where table_schema='{dataSource}';";

            DataTable dt = _sqlSugarClient.Ado.GetDataTable(sql);


            for (int i = 0; i < tbNames.Rows.Count; i++)
            {
                DataTableDto table = new DataTableDto();
                table.TableName = tbNames.Rows[i]["TABLE_NAME"].ToString();
                table.TableType = tbNames.Rows[i]["TYPE"].ToString();
                table.TableID = (i + 1).ToString();
                table.TableExplain = tbNames.Rows[i]["TABLE_COMMENT"].ToString();
                DataRow[] drs = dt.Select(string.Format("TABLE_NAME = '{0}'", table.TableName));
                if (drs.Length == 0)
                {
                    continue;
                }

                table.TableCols = new List<TableColDto>();
                for (int j = 0; j < drs.Length; j++)
                {
                    TableColDto tblCol = new TableColDto();
                    tblCol.ColName = drs[j]["COLUMN_NAME"].ToString();
                    tblCol.ColType = drs[j]["DATA_TYPE"].ToString();
                    tblCol.ColLength = drs[j]["CHARACTER_MAXIMUM_LENGTH"].ToString();
                    tblCol.DecimalLength = "";
                    //tblCol.decimalLength = drs[j]["小数位数"].ToString();
                    if (drs[j]["IS_NULLABLE"].ToString() == "NO")
                    {
                        tblCol.ColNull = false;
                    }
                    else
                    {
                        tblCol.ColNull = true;
                    }
                    tblCol.ColDefault = drs[j]["COLUMN_DEFAULT"].ToString();
                    if (Convert.ToString(drs[j]["COLUMN_KEY"]) == "PRI")
                    {
                        tblCol.ISKey = true;
                    }
                    else
                    {
                        tblCol.ISKey = false;
                    }
                    tblCol.ColExp = drs[j]["COLUMN_COMMENT"].ToString();
                    table.TableCols.Add(tblCol);
                    if (tblCol.ISKey)
                    {
                        if (!string.IsNullOrWhiteSpace(table.KeyName))
                        {
                            table.KeyName += ";";
                        }
                        table.KeyName += tblCol.ColName;
                    }

                }
                rtn.Add(table);
            }

            return rtn;
        }
    }
}
