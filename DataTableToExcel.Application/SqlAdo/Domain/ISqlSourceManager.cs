﻿using System.Collections.Generic;
using DataTableToExcel.Application.Shared.SqlAdo.SqlSourceDtos;

namespace DataTableToExcel.Application.SqlAdo
{
    public interface ISqlSourceManager
    {
        List<string> GetDataSourceList();

        List<DataTableDto> GetTableMsg(string DataSource);
    }
}
