﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataTableToExcel.Entity.Admin
{
    public class DataList
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DataSource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Ds_Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Ds_Pwd { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FileName { get; set; }


    }
}
