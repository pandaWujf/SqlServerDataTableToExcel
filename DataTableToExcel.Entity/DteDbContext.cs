﻿using DataTableToExcel.Entity.Admin;
using Microsoft.EntityFrameworkCore;

namespace DataTableToExcel.Entity
{
    public class DteDbContext : DbContext
    {
        public DbSet<DataList> DataLists { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=Dte.db");
            optionsBuilder.UseLazyLoadingProxies();
            base.OnConfiguring(optionsBuilder);
        }
    }
}
