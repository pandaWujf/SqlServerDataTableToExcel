﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace DataTableToExcel.Controls.DataConverters
{
    public class NullObjectToCollapsedConverter : IValueConverter
    {
        private const string VisibilityCollapsed = "Collapsed";
        private const string VisibilityShow = "Visible";

        // 当值从绑定源传播给绑定目标时，调用方法Convert
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return VisibilityCollapsed;
            }
            else
            {
                return VisibilityShow;
            }
        }

        // 当值从绑定目标传播给绑定源时，调用此方法ConvertBack
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

    }
}
