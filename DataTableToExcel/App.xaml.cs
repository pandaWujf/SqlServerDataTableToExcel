﻿using System.Windows;
using DataTableToExcel.ViewModels;
using DataTableToExcel.Views;
using Prism.Ioc;
using Prism.Unity;

namespace DataTableToExcel
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {
        public App()
        {

        }

        protected override Window CreateShell()
        {
            return Container.Resolve<Main>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterDialog<ItemCardEdit, ItemCardEditViewModel>();
        }
    }
}
