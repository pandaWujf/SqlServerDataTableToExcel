﻿using DataTableToExcel.Application.Admin;
using DataTableToExcel.Application.Exports;
using DataTableToExcel.Application.SqlAdo;
using DataTableToExcel.Models;
using Microsoft.Win32;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace DataTableToExcel.ViewModels
{
    public class MainViewModel : BindableBase
    {
        public IDialogService _dialogService;
        private DataListSerivce dataListSerivce;
        private SqlSourceService sqlSourceService;
        private ExportToExcelService exportToExcelService;

        private ObservableCollection<DataItemModel> _dataDictionaries;

        public ObservableCollection<DataItemModel> DataDictionaries
        {
            get => _dataDictionaries;
            set => SetProperty(ref _dataDictionaries, value);
        }


        public MainViewModel(IDialogService dialogService)
        {
            exportToExcelService = new ExportToExcelService();


            dataListSerivce = new DataListSerivce();
            _dialogService = dialogService;

            var dataList = dataListSerivce.GetAll();
            var itmes = dataList.Select(a => new DataItemModel()
            {
                Id = a.Id,
                Name = a.Name,
                Type = a.Type,
                IpAddress = a.IpAddress,
                DataSource = a.DataSource,
                Ds_Name = a.Ds_Name,
                Ds_Pwd = a.Ds_Pwd,
                FileName = a.FileName

            }).ToList();


            DataDictionaries = new ObservableCollection<DataItemModel>(itmes);

            DataDictionaries.Add(new DataItemModel());
        }

        public DelegateCommand AddCmd => new Lazy<DelegateCommand>(() => new DelegateCommand(() => Add())).Value;

        private void Add()
        {
            _dialogService.ShowDialog("ItemCardEdit", new DialogParameters($"dataItem="), r =>
            {
                if (r != null && r.Result == ButtonResult.OK)
                {
                    var result = r.Parameters.GetValue<string>("dataItem");
                    var rtnObj = JsonConvert.DeserializeObject<DataItemModel>(result);
                    var count = DataDictionaries.Count;
                    DataDictionaries.Insert(count - 1, rtnObj);
                }
            });
        }

        public DelegateCommand<DataItemModel> EditCmd => new Lazy<DelegateCommand<DataItemModel>>(() => new DelegateCommand<DataItemModel>((e) => Edit(e))).Value;

        private void Edit(DataItemModel e)
        {
            var dataItem = JsonConvert.SerializeObject(e);
            _dialogService.ShowDialog("ItemCardEdit", new DialogParameters($"dataItem={dataItem}"), r =>
            {
                if (r != null && r.Result == ButtonResult.OK)
                {
                    var result = r.Parameters.GetValue<string>("dataItem");

                    if (string.IsNullOrWhiteSpace(result))
                    {
                        return;
                    }

                    var rtnObj = JsonConvert.DeserializeObject<DataItemModel>(result);
                    var index = DataDictionaries.IndexOf(e);
                    DataDictionaries[index] = rtnObj;
                }
            });
        }

        public DelegateCommand<DataItemModel> DeleteCmd => new Lazy<DelegateCommand<DataItemModel>>(() => new DelegateCommand<DataItemModel>((e) => Delete(e))).Value;

        private void Delete(DataItemModel e)
        {
            if (MessageBox.Show("确定要删除吗?", "提醒", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                dataListSerivce.Delete(e.Id.Value);
                DataDictionaries.Remove(e);
            }
        }


        public DelegateCommand<DataItemModel> ExportCmd => new Lazy<DelegateCommand<DataItemModel>>(() => new DelegateCommand<DataItemModel>((e) => Export(e))).Value;

        private void Export(DataItemModel e)
        {
            try
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Title = "选择Excel";
                dlg.FileName = e.FileName; // Default file name
                dlg.DefaultExt = ".xls"; // Default file extension
                dlg.Filter = "Excel文件|*.xls"; // Filter files by extension
                                              // dlg.InitialDirectory = initFolder;

                // Process save file dialog box results
                if (dlg.ShowDialog() == true)
                {
                    var fileName = dlg.FileName;

                    sqlSourceService = new SqlSourceService(e.Type, e.IpAddress, e.Ds_Name, e.Ds_Pwd);

                    var result = sqlSourceService.GetTableMsg(e.DataSource);
                    exportToExcelService.ExportToExcel(result, fileName);
                    dataListSerivce.UpdateFileName(e.Id.Value, fileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

    }
}
