﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using DataTableToExcel.Application.Admin;
using DataTableToExcel.Application.Shared.Admin.DataListDtos;
using DataTableToExcel.Application.Shared.Common;
using DataTableToExcel.Application.SqlAdo;
using DataTableToExcel.Models;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace DataTableToExcel.ViewModels
{
    public class ItemCardEditViewModel : BindableBase, IDialogAware
    {
        private ObservableCollection<string> types;


        private SqlSourceService sqlSourceService;
        private DataListSerivce dataListSerivce;

        public ObservableCollection<string> Types
        {
            get => types;
            set => SetProperty(ref types, value);
        }

        private DataItemModel dataItem;

        public DataItemModel DataItem
        {
            get => dataItem;
            set => SetProperty(ref dataItem, value);
        }


        private string title;

        public string Title
        {
            get => title;
            set => SetProperty(ref title, value);
        }


        private ObservableCollection<string> dbList;

        public ObservableCollection<string> DbList
        {
            get => dbList;
            set => SetProperty(ref dbList, value);
        }


        public ItemCardEditViewModel()
        {

            dataListSerivce = new DataListSerivce();

            Types = new ObservableCollection<string>();
            Types.Add(TypeConsts.SqlServer);
            Types.Add(TypeConsts.MySql);

            DataItem = new DataItemModel();
            DataItem.Type = Types[0];
        }


        public DelegateCommand<PasswordBox> GetDbListCmd => new Lazy<DelegateCommand<PasswordBox>>(() => new DelegateCommand<PasswordBox>((e) => GetDbList(e))).Value;

        private void GetDbList(PasswordBox e)
        {
            DataItem.Ds_Pwd = e.Password;
            GetDbListFunc();
        }

        private void GetDbListFunc()
        {
            sqlSourceService = new SqlSourceService(DataItem.Type, DataItem.IpAddress, DataItem.Ds_Name, DataItem.Ds_Pwd);

            var dataList = sqlSourceService.GetDataSourceList();

            DbList = new ObservableCollection<string>(dataList);

            if (DbList != null && DbList.Count != 0)
            {
                if (!DbList.Contains(DataItem.DataSource))
                {
                    DataItem.DataSource = DbList[0];
                }
            }
            else
            {
                DataItem.DataSource = string.Empty;
            }
        }


        public DelegateCommand SaveCmd => new Lazy<DelegateCommand>(() => new DelegateCommand(() => Save())).Value;

        private void Save()
        {
            CreateOrEditDataListDto input = new CreateOrEditDataListDto();
            input.Id = DataItem.Id;
            input.Name = DataItem.Name;
            input.Type = DataItem.Type;
            input.IpAddress = DataItem.IpAddress;
            input.DataSource = DataItem.DataSource;
            input.Ds_Name = DataItem.Ds_Name;
            input.Ds_Pwd = DataItem.Ds_Pwd;
            input.FileName = DataItem.FileName;

            var dataList = dataListSerivce.CreateOrUpdate(input);
            DataItem.Id = dataList.Id;
            IDialogResult dialogResult = new DialogResult(ButtonResult.OK);
            var paremeter = JsonConvert.SerializeObject(DataItem);
            dialogResult.Parameters.Add("dataItem", paremeter);

            RequestClose.Invoke(dialogResult);
        }

        public bool CanCloseDialog()
        {
            return true;
        }
        public void OnDialogClosed()
        {

        }

        public event Action<IDialogResult> RequestClose;

        public void OnDialogOpened(IDialogParameters parameters)
        {
            var input = parameters.GetValue<string>("dataItem");
            if (string.IsNullOrWhiteSpace(input))
            {
                Title = "增加";

                DataItem = new DataItemModel();
                DataItem.Type = Types[0];
                DbList = new ObservableCollection<string>();
            }
            else
            {
                Title = "修改";

                DataItem = JsonConvert.DeserializeObject<DataItemModel>(input);

                GetDbListFunc();
            }
        }
    }
}
