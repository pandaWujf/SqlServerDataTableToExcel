﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DataTableToExcel.Models;

namespace DataTableToExcel.Views
{
    /// <summary>
    /// Item.xaml 的交互逻辑
    /// </summary>
    public partial class ItemCardShow : UserControl
    {
        public ItemCardShow()
        {
            InitializeComponent();
        }

        [Category("Extend Properties")]
        public DataItemModel DataItem
        {
            get => (DataItemModel)GetValue(DataItemProperty);

            set => SetValue(DataItemProperty, value);
        }

        public static readonly DependencyProperty DataItemProperty = DependencyProperty.Register("DataItem", typeof(DataItemModel), typeof(ItemCardShow), new PropertyMetadata(new DataItemModel(), DataItemChange));


        private static void DataItemChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var itemCardShow = d as ItemCardShow;
            if (itemCardShow == null)
            {
                return;
            }

            itemCardShow.TxtNameType.Text = itemCardShow.DataItem.Type;
            itemCardShow.TxtName.Text = itemCardShow.DataItem.Name;
            itemCardShow.TxtIpAddress.Text = itemCardShow.DataItem.IpAddress;
            itemCardShow.TxtDataSource.Text = itemCardShow.DataItem.DataSource;
        }


        [Category("Extend Properties")]
        public ICommand DeleteCommand
        {
            get { return (ICommand)GetValue(DeleteCommandProperty); }
            set { SetValue(DeleteCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedItems.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DeleteCommandProperty =
            DependencyProperty.Register("DeleteCommand", typeof(ICommand), typeof(ItemCardShow), null);


        [Category("Extend Properties")]
        public ICommand EditCommand
        {
            get { return (ICommand)GetValue(EditCommandProperty); }
            set { SetValue(EditCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedItems.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EditCommandProperty =
            DependencyProperty.Register("EditCommand", typeof(ICommand), typeof(ItemCardShow), null);


        [Category("Extend Properties")]
        public ICommand ExportCommand
        {
            get { return (ICommand)GetValue(ExportCommandProperty); }
            set { SetValue(ExportCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedItems.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ExportCommandProperty =
            DependencyProperty.Register("ExportCommand", typeof(ICommand), typeof(ItemCardShow), null);


        private void BtnExport_Click(object sender, RoutedEventArgs e)
        {
            if (ExportCommand != null)
            {
                if (ExportCommand.CanExecute(DataItem))
                {
                    ExportCommand.Execute(DataItem);
                }
            }
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (EditCommand != null)
            {
                if (EditCommand.CanExecute(DataItem))
                {
                    EditCommand.Execute(DataItem);
                }
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (DeleteCommand != null)
            {
                if (DeleteCommand.CanExecute(DataItem))
                {
                    DeleteCommand.Execute(DataItem);
                }
            }
        }
    }
}
