﻿using Prism.Mvvm;
using System;

namespace DataTableToExcel.Models
{
    public class DataItemModel : BindableBase
    {
        private Guid? id;
        public Guid? Id
        {
            get => id;
            set => SetProperty(ref id, value);
        }

        public DateTime CreateDate { get; set; }

        private string name;
        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        private string type;
        public string Type
        {
            get => type;
            set => SetProperty(ref type, value);
        }

        private string ipAddress;
        public string IpAddress
        {
            get => ipAddress;
            set => SetProperty(ref ipAddress, value);
        }

        private string dataSource;
        public string DataSource
        {
            get => dataSource;
            set => SetProperty(ref dataSource, value);
        }

        private string ds_Name;
        public string Ds_Name
        {
            get => ds_Name;
            set => SetProperty(ref ds_Name, value);
        }

        private string ds_Pwd;
        public string Ds_Pwd
        {
            get => ds_Pwd;
            set => SetProperty(ref ds_Pwd, value);
        }

        private string fileName;
        public string FileName
        {
            get => fileName;
            set => SetProperty(ref fileName, value);
        }
    }
}
